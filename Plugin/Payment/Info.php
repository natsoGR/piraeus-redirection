<?php

namespace Natso\Piraeus\Plugin\Payment;

class Info extends \Magento\Framework\View\Element\Template {

	protected $_order;

	public function __construct (
		\Magento\Sales\Model\OrderFactory $_orderFactory,
		\Magento\Framework\View\Element\Template\Context $context,
    	$data = []
	)
	{
		$this->_order = $_orderFactory->create();
		parent::__construct($context, $data);
	}

    public function afterGetSpecificInformation(\Magento\Payment\Block\Info $subject) {

 		$orderId = $this->getRequest()->getParam('order_id');

 		if ( !empty($orderId) ) {
	 		$ai = $this->_order->load($orderId)->getPayment()->getAdditionalInformation();
	 		if ( isset($ai['installments']) ) {
	 			return array('Installments' => $ai['installments']);
	 		}
 		}
    }
}